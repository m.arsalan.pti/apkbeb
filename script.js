const arscountdown = document.querySelector('.arscountdown');
const downloadBtn = document.querySelector('.arsdownload-btn');
const arscircle = document.querySelector('.arscircle'); 
let timeLeft = 5;
const timer = setInterval(() => {
  arscountdown.textContent = timeLeft;
  timeLeft--;
  if (timeLeft === 0) {
    clearInterval(timer);
    downloadBtn.style.display = 'block';
    arscircle.style.opacity = 0;
  }
}, 800);